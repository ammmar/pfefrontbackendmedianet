import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {QuestionService} from '../../../services/question.service';
@Component({
  selector: 'app-addquestion',
  templateUrl: './addquestion.component.html',
  styleUrls: ['./addquestion.component.scss']
})
export class AddquestionComponent implements OnInit {
  addquestion: FormGroup;
  idTest:any;

  constructor(private httpClient: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, private  fb: FormBuilder, private questionService: QuestionService) {
    this.idTest=this.activatedRoute.snapshot.params['id'];
    console.log("**********AddQuestion: id  routed  "+this.idTest);
  }

  ngOnInit() {
    this.addquestion = this.fb.group({
      desgnQuestion: ['', Validators.required],
      tempsQuestion: ['', Validators.required],
      unite: ['', Validators.required],

    });
  }

  onSubmit() {
    if (this.addquestion.valid) {
      console.log("valide form");

      this.questionService.ajouterQuestion(this.addquestion.value,this.idTest).toPromise().then(response => {

          console.log("resultat add test " + response);

          Swal.fire({
            title: 'add question success  ',
            text: 'success',
            icon: 'success',
          });
          if(this.idTest===0)
          {
            this.idTest=2;
          }
          this.router.navigate(['/question-test/',this.idTest]);

        }
      ,error=>{
          Swal.fire({
            title: 'add question failure ',
            text: 'probleme d ajout question',
            icon: 'warning',
          });
          console.log("error de ajouter de test "+JSON.stringify(error));
        }

      );


    }
  }

  NavToQuestionTest(){
    this.router.navigate(['/question-test',this.idTest])
  }
}
