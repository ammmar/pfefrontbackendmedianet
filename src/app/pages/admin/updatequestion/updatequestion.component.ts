import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {QuestionService} from '../../../services/question.service';

@Component({
  selector: 'app-updatequestion',
  templateUrl: './updatequestion.component.html',
  styleUrls: ['./updatequestion.component.scss']
})
export class UpdatequestionComponent implements OnInit {

  updateform: FormGroup;
  idQuestion: any;
  Question: any;

  constructor(private httpClient: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, private  fb: FormBuilder, private questionService: QuestionService) {
    this.idQuestion = this.activatedRoute.snapshot.params['id'];
    console.log("**********UpdateQuestion: id  routed  " + this.idQuestion);

    this.questionService.getQuestionById(this.idQuestion).toPromise().then(response=>{
      console.log("UpdateQuestion   response is "+response);
      this.Question=JSON.parse(JSON.stringify(response));
      console.log("question  theme "+this.Question['desgnQuestion'])

    },error=>{
      console.log("UpdateQuestion:Error"+JSON.stringify(error));
    });
  }

  ngOnInit() {
    this.updateform = this.fb.group({
      desgnQuestion: ['', Validators.required],
      tempsQuestion: ['', Validators.required],
      unite: ['', Validators.required],

    });
  }

  onSubmit() {
   if (this.updateform.valid) {
      console.log("valide form");
      this.updateform.value["idQuestion"]=this.idQuestion;
      this.questionService.creerModifierQuestion(this.updateform.value).toPromise().then(reponse=>{
        Swal.fire({
          title: 'modifier question success  ',
          text: 'success',
          icon: 'success',
        });
      //  this.router.navigate(['/question-test',this.idQuestion])
        console.log("UpdateQuestion:Sucess"+JSON.stringify(reponse));
      },error=>{

        Swal.fire({
          title: 'modifier test echec  ',
          text: 'failure',
          icon: 'warning',
        });
      }
    // console.log("UpdateQuestion:error"+JSON.stringify(error));

   )

    }

  }
}
