import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {UserService} from '../../../services/user.service';
import {CandidatService} from '../../../services/candidat.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  today=new Date();
  poste;
  email:any;
  showMessages: any = {};
  submitted: boolean = false;
   listRole:any[]=["Admin","RH","ChefDepartement"];
  validation = {};
  addUser:FormGroup;
  constructor(private  userService:UserService,private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder) {


  }


  ngOnInit() {
    this.addUser=this.fb.group({
      username:['',Validators.required],
      password:['',Validators.required],
      firstname:['',Validators.required],
      lastname:['',Validators.required],
      email:['',Validators.required],
      usertel:['',Validators.required],
      useradresse:['',Validators.required]
    });
  }

  OnReset(){
    this.addUser.reset();
  }

  NavToList(){
    this.router.navigate(["manage-user"])
  }


  RoleSelected($event: MatSelectChange) {
    this.poste=$event.source.value;
    console.log("role is ==>"+this.poste);
  }

onSubmit(){
if(this.addUser.valid){
    this.addUser.value["userdate"]=this.today;
  this.addUser.value["lastPasswordResetDate"]=this.today;
  this.addUser.value["disponibilite"]=true;
  this.addUser.value["enabled"]=true;
  this.addUser.value["poste"]=this.poste;
    this.userService.creerUser(this.addUser.value).toPromise().then(reponse=>{
      Swal.fire({
        title: 'add user success  ',
        text: 'success',
        icon: 'success',
      });
      this.router.navigate(["manage-user"])
    },error=>{
      Swal.fire({
        title: 'add user add  ',
        text: 'failed add user',
        icon: 'warning',
      });
    });


}

}
}
