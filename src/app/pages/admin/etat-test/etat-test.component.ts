import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {HttpClient} from '@angular/common/http';
import {TestService} from '../../../services/test.service';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Test} from '../../../models/test';
import {MailComposeDialogComponent} from '../../../Modal/mail-compose-dialog/mail-compose-dialog.component';
import Swal from "sweetalert2";
import {SharedService} from '../../../services/shared.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-etat-test',
  templateUrl: './etat-test.component.html',
  styleUrls: ['./etat-test.component.scss']
})
export class EtatTestComponent implements OnInit {

  /*
  "idTest": 2,
        "dateDebut": "2020-05-03",
        "dateEvaluation": null,
        "dateFin": "2020-05-11",
        "description": "HTML5",
        "etatTest": 0,
        "logo": null,
        "score": 0,
        "theme": "html5",
        "typeTest": "Technique",
        "nomcandidat": "morad",
        "id_candidat": 3,
        "prenomcandidat": "bouzid"
   */
  //          const searchStr = (test.idTest + test.theme  + test.dateDebut + test.dateFin+test.score+test.description+test.nomcandidat+test.prenomcandidat+test.etatTest).toLowerCase();
  displayedColumns = ['theme', 'dateDebut','dateFin',  'score', 'nom','prenom','etatTest','actions'];
  exampleDatabase: TestService| null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number;



  constructor(public httpClient: HttpClient,
              public dialog: MatDialog,public testService: TestService,private  sharedService:SharedService,private  router:Router) {

  }
  //********* dialog
  openDialogMail(name:any): void {
    //this.afficheMsg("Dialog  id question"+id_question+" name question"+namequestion);
    const dialogRef = this.dialog.open(MailComposeDialogComponent, {
      width: '700px',
      height: '500px',
      data:{name:name}


    });
    this.affichemsg("AffecterDialog open");

    dialogRef.afterClosed().subscribe(res => {
      console.log("AffecterDialog closed");

    });
  }


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter',  {static: true}) filter: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }
  affichemsg(msg:any){
    console.log("MAIL COMPOSE DIALOG ***********"+msg);
  }

  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }

//voir Resultat test
  voirResultat(resultat:any,score:string,nameCandidat:string,dateEval:string,theme:string){
   // this.affichemsg("resultat is "+JSON.stringify(resultat));
    this.sharedService.changeMessage(resultat);
    this.affichemsg("score "+score+"  name "+nameCandidat+"date Eval"+dateEval);

    localStorage.setItem("score",score);
    localStorage.setItem("name",nameCandidat);
    localStorage.setItem("dateEval",dateEval);
    localStorage.setItem("theme",theme);
    this.router.navigate(['/voirResultat']);
  }

  deleteTest(id:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Voulez vous supprimer ',
      text: "ce Test",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Voulez vous supprimer!',
      cancelButtonText: 'Non!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {
        this.testService.deleteTest(id).toPromise().then(reponse=>{
          console.log("result de suprression"+JSON.stringify(reponse));
          swalWithBootstrapButtons.fire(
            'Supprimer!',
            'l operation a été effectué  .',
            'success')
        },error=>{
          swalWithBootstrapButtons.fire(
            'Probleme',
            'Probelem de suppression ',
            'warning')
        })




      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Anuller',
          'votre test est en cours  :)',
          'error'
        )
      }
    })
  }

  public loadData() {
    this.exampleDatabase = new TestService(this.httpClient);
    this.testService.getAllTestEtat();
    console.log("data server "+JSON.stringify(this.testService.gettetsme()));
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }


}



export class ExampleDataSource extends DataSource<Test> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Test[] = [];
  renderedData: Test[] = [];

  constructor(public _exampleDatabase: TestService,
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Test[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._exampleDatabase.getAllTestEtat()

    console.log("dataService" + this._exampleDatabase.getAllTestEtat())
    /*
    id:number;
    theme:string;
    description:string;
    logo:string;
    dateDebut:string;
    dateFin:string;
    estOuvert:number;
    */

    return merge(...displayDataChanges).pipe(map(() => {
        // Filter data


        this.filteredData = this._exampleDatabase.data.slice().filter((test: Test) => {
          const searchStr = (test.idTest + test.theme + test.dateFin + test.score +test.description + test.nomcandidat+ test.prenomcandidat+ test.etatTestInfo+test.numTel+test.resultat+test.dateEvaluation).toLowerCase();
          return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
        });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
        return this.renderedData;
      }
    ));
  }

  disconnect() {
  }


  /** Returns a sorted copy of the database data. */
  sortData(data: Test[]): Test[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    /*
        return data.sort((a, b) => {
          let propertyA: number | string = '';
          let propertyB: number | string = '';

          switch (this._sort.active) {
            case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
            case 'title': [propertyA, propertyB] = [a.title, b.title]; break;
            case 'state': [propertyA, propertyB] = [a.state, b.state]; break;
            case 'url': [propertyA, propertyB] = [a.url, b.url]; break;
            case 'created_at': [propertyA, propertyB] = [a.created_at, b.created_at]; break;
            case 'updated_at': [propertyA, propertyB] = [a.updated_at, b.updated_at]; break;
          }

          const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
          const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

          return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });*/
  }
}
