import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import 'style-loader!angular2-toaster/toaster.css';
import {NbDialogService} from "@nebular/theme";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
/*
Documentation for sweet alert
https://sweetalert2.github.io/
 */

import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
} from '@nebular/theme';
import {ToasterService} from "angular2-toaster";
import { ToasterConfig } from 'angular2-toaster';
import * as moment from 'moment';
import { TestService } from 'app/services/test.service';

export const formErrors: { [key: string]: string } = {
  required: 'This is a required field',
  pattern: 'Email must be a valid email address (example@email.com).',
  minLength: 'Password must contain at least 8 characters.',
  minLengthPhone: 'Phone Number must contain at least 8 characters.',

  mismatch: 'Passwords don\'t match.',
  unique: 'Passwords must contain at least 3 unique characters.'
};



@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.scss']
})
export class AddTestComponent implements OnInit {
listType: Array<string> = ["Test Technique","Test logique"];

  constructor(private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder,private testService:TestService) {


  }


  addtest:FormGroup;
  submitted = false;
  today=new Date();
  //file declarationj
  private selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;

  private uploadForm: any;
  httpErrors: string;
  formErrors = formErrors;

  config: ToasterConfig;
  index = 1;
  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';


  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];
  title = 'HI there!';
  content = `I'm cool toaster!`;
  positions: string[] = [
    NbGlobalPhysicalPosition.TOP_RIGHT,
    NbGlobalPhysicalPosition.TOP_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_LEFT,
    NbGlobalPhysicalPosition.BOTTOM_RIGHT,
    NbGlobalLogicalPosition.TOP_END,
    NbGlobalLogicalPosition.TOP_START,
    NbGlobalLogicalPosition.BOTTOM_END,
    NbGlobalLogicalPosition.BOTTOM_START,
  ];

  quotes = [
    { title: null, body: 'We rock at Angular' },
    { title: null, body: 'Titles are not always needed' },
    { title: null, body: 'Toastr rock!' },
  ];
  private currentDate: moment.Moment;
  isAfter: any;

  makeToast() {
    this.showToast(this.status, this.title, this.content);
  }

  openRandomToast () {
    const typeIndex = Math.floor(Math.random() * this.types.length);
    const quoteIndex = Math.floor(Math.random() * this.quotes.length);
    const type = this.types[typeIndex];
    const quote = this.quotes[quoteIndex];

    this.showToast(type, quote.title, quote.body);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';
/*
    this.nbToastrService.show(
      body,
      `Message : ${titleContent}`,
      config);*/
  }



  ngOnInit() {
 /*   this.uploadForm = this.fb.group({
      file: ['']
    });*/
    this.addtest=this.fb.group({
      theme:['',Validators.required],
      description:['',Validators.required],
      dateDebut:['',Validators.required],
      dateFin:['',Validators.required],
      typeTest:['',Validators.required]
    });
  }


//Gets called when the user selects an image
public onFileChanged(event) {
  //Select File
  this.selectedFile = event.target.files[0];
  const uploadData = new FormData();
  uploadData.append('myFile', this.selectedFile, this.selectedFile.name);

}



  typeSelected($event: MatSelectChange) {
    this.addtest.patchValue({
      typeTest:$event.value
    })
  }


  onReset() {
    this.addtest.reset();
  }

  onSubmit() {
    if(this.addtest.valid) {
      //  console.log("value form "+this.addtest.value["theme"]+"des"+this.addtest.value["description"]);
      let id = 1;
      //this.testService.ajouterTest(this.addtest.value,1);
      this.testService.ajouterTest(this.addtest.value, 2).toPromise().then(response => {

        console.log("resultat add test " + response);


        Swal.fire({
          title: 'add test success  ',
          text: 'success',
          icon: 'success',
        });
        this.router.navigate(['/test/']);

      }, error1 => {
        Swal.fire({
          title: 'add test failure ',
          text: 'probleme d ajout test',
          icon: 'warning',
        });
        console.log("addTest:Erreur is =========>***"+JSON.stringify(error1));

      });

    }
    else{
      console.log("problemme de ajout de test ")
    }
  }
  addTest(){
    if(this.addtest.valid)
    {
      console.log("value form "+this.addtest.value);

     // this.testService.addTest(this)
    }
  }

}
