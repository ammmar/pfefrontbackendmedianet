import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import { TestService } from 'app/services/test.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {QuestionChoixComponent } from 'app/Modal/question-choix/question-choix.component';
import Swal from 'sweetalert2';
import {QuestionService} from '../../../services/question.service';
@Component({
  selector: 'app-gestionquestion',
  templateUrl: './gestionquestion.component.html',
  styleUrls: ['./gestionquestion.component.scss']
})
export class GestionquestionComponent implements OnInit {

 // myControls = new FormControl();
 filteredOptions: Observable<string[]>
 idTest:any;
  listQuestion:[]=[];
 myControl = new FormControl();
 //test:FormGroup;
 panelOpenState: boolean=false;
 options:[] = [];
  name: string;
  color: string;
  public searchText : string;


 constructor(private httpClient: HttpClient,private testService:TestService,private router:Router,private activatedRoute: ActivatedRoute,public dialog: MatDialog,private questionService:QuestionService) {
  this.idTest=this.activatedRoute.snapshot.params['id'];
  console.log("id routed  "+this.idTest);
  this.testService.getQuestionByTestId(this.idTest).toPromise().then(response=>{
    console.log("qestionQuestionresponse is "+response)
    this.listQuestion=(JSON.parse(JSON.stringify(response)));
    for(let i=0;i<this.listQuestion.length;i++)
    {
      let q=this.listQuestion[i];
        console.log("id question"+q["idQuestion"]+"desgn"+q["desgnQuestion"]);
    }
  });

     //console.log("Response test result is ",res);
     //this.ListTest=response;



 }

//********* dialog
  openDialog(id_question:any,namequestion:any): void {
   //this.afficheMsg("Dialog  id question"+id_question+" name question"+namequestion);
    const dialogRef = this.dialog.open(QuestionChoixComponent, {
      width: '700px',
      height:'500px',
      data:{id_question:id_question,name:namequestion}

  });
    console.log("Dialog opne");

    dialogRef.afterClosed().subscribe(res => {
      console.log("Dialog closed");
    });
  }

  deleteDialog(idQuestion:any){

    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'voulez vous supprimer cette question ?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Voulez vous supprimer ',
      cancelButtonText: 'No, cancel   !',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
       this.questionService.DeleteQuestion(idQuestion).toPromise().then(reponse=>{
         swalWithBootstrapButtons.fire(
           'Deleted!',
           'votre question est supprimé',
           'success'
         )
       },error=>{
         swalWithBootstrapButtons.fire(
           'Probeleme de suppression',
           'votre question est supprim',
           'warning'
         )
       });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'votre question est en cours :)',
          'error'
        )
      }
    })







  }

 ngOnInit() {
 }

  NaToUpdateQuestion(idQuestion:any){
  this.afficheMsg("id_question"+idQuestion);
    this.router.navigate(['/update-question',idQuestion])
  }

 navAddTest() {
   this.router.navigate(['/test/add']);
 }
  navAddquestion(){
    this.router.navigate(['/addquestion/',this.idTest]);
  }

  afficheMsg(msg:any){
   console.log("*****GestionQuestionComponents******"+msg);
  }
}
