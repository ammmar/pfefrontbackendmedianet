import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulerTestComponent } from './simuler-test.component';

describe('SimulerTestComponent', () => {
  let component: SimulerTestComponent;
  let fixture: ComponentFixture<SimulerTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulerTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulerTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
