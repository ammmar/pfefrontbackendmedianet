import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule, ToastrService } from "ngx-toastr";

import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import {AppRoutes} from './app.routing';
//Tutorial
//https://sweetalert2.github.io/

//chat tutourial  https://www.yamicode.com/snippets/real-time-chat-angular-spring-boot-java-websocket-stompjs

import {
  NbDialogModule,
   NbDialogService,
 } from '@nebular/theme';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import {HttpClientModule} from '@angular/common/http';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {ChartistModule} from 'ng-chartist';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import {CommonModule} from '@angular/common';
import {AuthGuardService} from './auth/auth-guard.service';
import {AuthService} from './auth/auth.service';
import {LoginActivate} from './auth/LoginActivate';
import { EtatTestComponent } from './pages/admin/etat-test/etat-test.component';
import { MailComposeDialogComponent } from './Modal/mail-compose-dialog/mail-compose-dialog.component';
import { VoirResultatComponent } from './pages/admin/voir-resultat/voir-resultat.component';
import { ListUserComponent } from './pages/admin/list-user/list-user.component';
import { AddUserComponent } from './pages/admin/add-user/add-user.component';
import { UpdateUserComponent } from './pages/admin/update-user/update-user.component';



@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,




  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    ChartsModule,

    RouterModule.forRoot(AppRoutes,{
      useHash: true
    }),
    SidebarModule,
    NbDialogModule.forRoot(),
    NavbarModule,
    ToastrModule.forRoot(),
    FooterModule,
    FixedPluginModule
  ],
  providers:[NbDialogService,ToastrService],
  bootstrap: [AppComponent]
})
export class AppModule { }
