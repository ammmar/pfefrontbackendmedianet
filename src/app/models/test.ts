//displayedColumns = ['id', 'theme', 'description', 'Logo', 'date Debut', 'Date Fin ', 'Est Ouvert'];
export class Test {
  idTest: number;
  dateDebut: string;
  dateEvaluation: string;
  dateFin: string;
  description: string;
  etatTest: number;
  logo: any;
  score: number;
  numTel:string;
  theme: string;
  resultat: string;
  typeTest: string;
  etatTestInfo:String;
  nomcandidat: string;
  id_candidat: number;
  prenomcandidat: string;

}
