import { Routes } from '@angular/router';
import { DashboardComponent } from '../../pages/admin/dashboard/dashboard.component';
import { UserComponent } from '../../pages/admin/user/user.component';
import { TableComponent } from '../../pages/admin/table/table.component';
import { TypographyComponent } from '../../pages/admin/typography/typography.component';
import { IconsComponent } from '../../pages/admin/icons/icons.component';
import { MapsComponent } from '../../pages/admin/maps/maps.component';
import { ListcandidatComponent }         from '../../pages/admin/listcandidat/listcandidat.component';
import { ListTestComponent}         from '../../pages/admin/list-test/list-test.component';
import { UpdateTestComponent } from 'app/pages/admin/update-test/update-test.component';
import { GestionquestionComponent } from 'app/pages/admin/gestionquestion/gestionquestion.component';
import { AddTestComponent } from '../../pages/admin/add-test/add-test.component';
import {AddquestionComponent} from '../../pages/admin/addquestion/addquestion.component';
import {UpdatequestionComponent} from '../../pages/admin/updatequestion/updatequestion.component';
import {AddCandidatComponent} from '../../pages/admin/add-candidat/add-candidat.component';
import {UpdatecandidatComponent} from '../../pages/admin/updatecandidat/updatecandidat.component';
import {SimulerTestComponent} from '../../pages/admin/simuler-test/simuler-test.component';
import {EtatTestComponent} from '../../pages/admin/etat-test/etat-test.component';
import {VoirResultatComponent} from '../../pages/admin/voir-resultat/voir-resultat.component';
import {ListUserComponent} from '../../pages/admin/list-user/list-user.component';
import {AddUserComponent} from '../../pages/admin/add-user/add-user.component';
import {UpdateUserComponent} from '../../pages/admin/update-user/update-user.component';
export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user',           component: UserComponent },
    { path: 'table',          component: TableComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
  //test routing
	{ path: 'modifier-test/:id', component: UpdateTestComponent},
    {  path :'test/add',component:AddTestComponent},
    {  path :'test',component:ListTestComponent},
    { path: 'question-test/:id',component:GestionquestionComponent},
  //question routing
  { path: 'addquestion/:id',component:AddquestionComponent},
  { path: 'update-question/:id',component:UpdatequestionComponent},
  //candiat routing
  { path: 'candidat', component: ListcandidatComponent },
  {path:'add-candidat',component:AddCandidatComponent},
  {path:'update-candidat/:id',component:UpdatecandidatComponent},
  //simuler test routing
  {path:'simulerTest/:id',component: SimulerTestComponent},
  //etat test
  {path:'etatTest',component: EtatTestComponent},
  {path:'voirResultat',component: VoirResultatComponent},
  //user CRUD
  {path:'manage-user',component: ListUserComponent}
,{path:'add-user',component:AddUserComponent}
  ,{path:'update-user',component:UpdateUserComponent}
];
//
    //{ path: 'modifier-test/:id',component: UpdateTestComponent},
