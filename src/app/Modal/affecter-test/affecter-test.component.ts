import {Component, OnInit, Inject, Output, EventEmitter} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";

import {NbDialogService} from "@nebular/theme";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";

import {MatRadioButton, MatRadioChange} from '@angular/material/radio';
import {ModelDataAffecterTestCanidiat} from '../data/model-data-affecterTest';
import {TestService} from '../../services/test.service';
import {CandidatService} from '../../services/candidat.service';
@Component({
  selector: 'app-affecter-test',
  templateUrl: './affecter-test.component.html',
  styleUrls: ['./affecter-test.component.scss']
})
export class AffecterTestComponent implements OnInit {

  affecterForm:FormGroup;
  id_test:any;
  idCandiat:any;
  nameTest:any;
  listCandidat:[]=[];
  today=new Date();
  isAfter:any;
  nameControl = new FormControl('');

  constructor(public dialogRef: MatDialogRef<AffecterTestComponent>, @Inject(MAT_DIALOG_DATA) public data: ModelDataAffecterTestCanidiat,private  fb: FormBuilder,private testService:TestService,private  candidatService:CandidatService)
  {
    this.nameTest=data.name;
    this.id_test=data.id_test;
      this.candidatService.getAllCandidat().toPromise().then(reponse=>{
          this.listCandidat=JSON.parse(JSON.stringify(reponse));
          this.afficherMessge("reulst "+JSON.stringify(reponse))
      },error=>{
      this.afficherMessge("error"+JSON.stringify(error));
      })
  }

  ngOnInit() {
    this.affecterForm=this.fb.group({
      dateDebut:['',Validators.required],
      dateFin:['',Validators.required]
    });
  }
  affecterCandidat(){







    if(this.id_test>0 && this.idCandiat>0) {

      if (this.affecterForm.valid) {
        let test={dateDebut:this.affecterForm.value["dateDebut"],dateFin:this.affecterForm.value["dateFin"]};
        this.afficherMessge("date debut" + this.affecterForm.value["dateDebut"]);
        this.testService.affecterTestCandidat(this.id_test, this.idCandiat, test).toPromise().then(reponse => {

          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'le candiat est affecté',
            showConfirmButton: false,
            timer: 1500
          })
        }, error => {
          Swal.fire({
            position: 'top-end',
            icon: 'warning',
            title: 'probleme d affectation',
            showConfirmButton: false,
            timer: 1500
          })
        })
      }
      this.dialogRef.close();
    }else{
      this.afficherMessge("acune id test et candiat exist"    );
    }
  }

  candidatSelected($event: MatSelectChange) {
    this.idCandiat=$event.source.value;
    this.afficherMessge("id candiat "+$event.source.value+" his name"+this.nameTest)
  }
  OnReset(){
    this.dialogRef.close();

  }


  afficherMessge(msg:any){
    console.log("affecterTest modal"+msg);
  }
}
