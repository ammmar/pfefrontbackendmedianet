import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from "sweetalert2";
import {ContactService} from '../../services/contact.service';
@Component({
  selector: 'app-mail-compose-dialog',
  templateUrl: './mail-compose-dialog.component.html',
  styleUrls: ['./mail-compose-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class MailComposeDialogComponent implements OnInit {
  composeForm: FormGroup;
  name: any;

  /**
   * Constructor
   *
   * @param {MatDialogRef<MailNgrxComposeDialogComponent>} matDialogRef
   * @param _data
   * @param {FormBuilder} _formBuilder
   */
  constructor(public  dialogRef: MatDialogRef<MailComposeDialogComponent>, @Inject(MAT_DIALOG_DATA) private _data: any, private _formBuilder: FormBuilder
    , private contactService: ContactService) {
    this.name = this._data.name;
    this.afficheMessage("name " + this._data.name);
    // Set the defaults

    this.composeForm = this.createComposeForm();
  }

  ngOnInit() {
  }

  createComposeForm(): FormGroup {
    return this._formBuilder.group({
      from: {
        value: ['ammar.hamza1995@gmail.com'],
        disabled: [true]
      },
      email: [''],
      subject: [''],
      name: [''],
      message: ['']
    });

  }

  afficheMessage(msg: any) {
    console.log("********** mail box dilaog****" + msg);
  }
  OnReset(){
    this.dialogRef.close();

  }
  sendMail() {
    if (this.composeForm.valid) {
      //this.afficheMessage("form value"+this.composeForm.value);

      this.contactService.SendMail(this.composeForm.value).toPromise().then(reponse => {
        Swal.fire({
          title: 'success email  sent ',
          text: 'success',
          icon: 'success',
        });
      },error=>{
        Swal.fire({
          title: 'mail non evoyeé  ',
          text: 'problem ',
          icon: 'success',
        });
      })


    }


  }
}
