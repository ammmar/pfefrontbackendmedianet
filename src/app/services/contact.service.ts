import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactService {


  private baseUrl = 'http://localhost:9093/auth/contact';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }


  SendMail(mail:any): Observable<Object>{
    return this.httpClient.post(`${this.baseUrl}/mail/`,mail);
  }
}
