import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  private baseUrl = 'http://localhost:9093/auth/question';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });

  }

  ajouterQuestion(question:any,id:any):Observable<Object> {
    return this.httpClient.put(`${this.baseUrl}/creerQuestion/${id}`,question);

  }

  creerModifierQuestion(question:any){
    return this.httpClient.post(`${this.baseUrl}/creerUpdateQuestion/`,question);

  }

  getQuestionById(id:any)
  {
    return this.httpClient.get(`${this.baseUrl}/getQuestiontById/${id}`);

  }

  getChoixByQuestionId(id:any)
  {
    return this.httpClient.get(`${this.baseUrl}/getChoixsByQuestionId/${id}`);

  }

  DeleteQuestion(id:any):Observable<Object>
  {
    return  this.httpClient.delete(`${this.baseUrl}/deletequestion/${id}`);
  }


  }

