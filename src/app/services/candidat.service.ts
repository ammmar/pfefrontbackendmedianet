import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CandidatService {
  private baseUrl = 'http://localhost:9093/auth/candidat';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });
  }
  getAllCandidat(){
    return this.httpClient.get(`${this.baseUrl}/getAllCandidat`);
  }
  getCandidatById(id:any):Observable<Object>{
    return  this.httpClient.get(`${this.baseUrl}/getCandidatById/${id}`);
  }
  creerModifierCandidat(candidat:any,id:any){
    return this.httpClient.put(`${this.baseUrl}/creerUpdateCandidat/${id}`,candidat);
  }

  creerCandidat(candidat:any)
  {
    return this.httpClient.post(`${this.baseUrl}/creerCandidat/`,candidat);

  }
  deleteCandidat(id:any)
  {
    return this.httpClient.delete(`${this.baseUrl}/delete/${id}`);

  }
}
