import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StatService {

  private baseUrl = 'http://localhost:9093/auth/stat';

  constructor(private httpClient:HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });


  }


  getstatInfo():Observable<Object>{
    return this.httpClient.get(`${this.baseUrl}/keyinfo`);
  }


  getEcoleStat():Observable<Object>{
    return this.httpClient.get(`${this.baseUrl}/ecolestat`);
  }
}
