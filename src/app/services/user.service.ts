import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = 'http://localhost:9093/user';

  constructor(private httpClient: HttpClient) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Access-Control-Allow-Methods": "GET, POST, DELETE, PUT"
    });
  }

  getAllUsers():Observable<Object> {
    return  this.httpClient.get(`${this.baseUrl}/getAllUser`);
  }
  deleteUser(id:any):Observable<Object>{
    return this.httpClient.delete(`${this.baseUrl}/deleteUser/${id}`);

  }

  creerUser(user:any){
    return this.httpClient.post(`${this.baseUrl}/adduser`,user);

  }
  updateUser(user:any ,id:any){
    return this.httpClient.put(`${this.baseUrl}/updateuser/${id}`,user);

  }
}
